#!/usr/bin/env python3
# Author: George Maysack-schlueter
# Description: Script for week 5 in python  class.

# Defining our degree conversion function - setting it to take one argument,
# which is the degrees in Fahrenheit to be converted
def convert_temp(degrees_fahrenheit):
    # degrees in Fahrenheit (argument) passed to equation to turn it to degrees
    # Celsius, which we then return the result of tto the program.
    degrees_celsius = (degrees_fahrenheit - 32) * 5 / 9
    return degrees_celsius
# Creating a main loop with an argument of 32 - which means 32 degrees
# Fahrenheit will be being passed as an argument when we run this script
# locally. The result is printed to the screen.
def main():
    print(convert_temp(32))

if __name__ == "__main__":
    main()
