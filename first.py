#!/bin/python3

# STRINGS
print("Hello World") # print string
print("""This line runs
on multiple lines""") # multi line
print("Two "+"strings!") # concatenate strings
print("\nTest New Line\n") # new line

# MATH
print(50 + 50) # add 
print(50 - 50) # subtract
print(50 * 50) # multiply
print(50 / 50) # divide
print(50 + 50 - 50 * 50 / 50) # pemdas
print(50 ** 2) # exponents
print(50 % 6) # modulo - remainder
print(50 / 6) # division w/ remainder (float)
print(50 // 6) # division no remainder
print("\n")

# VARIABLES
# AND
# METHODS
quote = "I love cheese pizza." # setting variable as string
print(quote) # printing variable

print(quote.upper()) # uppercase method
print(quote.lower()) # lowercase method
print(quote.title()) # title case method
print(len(quote)) # string length function

name = "Bob" # string
age = 22 # int
gpa = 3.8 # float - decimal

print(int(age)) # print variable as integer
print(int(30.1)) # print decimal - float as integer
print(int(30.9)) # int function does not round

# formatted string - including variables in strings. 
print(f"My name is {name}, I am {age} years old.\nMy GPA is {gpa}.")

age += 1 # adding 1 year to age
print(age) # printing increased age

birthday = 1 # variable set to integer
age += birthday # adding birthday variable to age variable
print(age) # printing updated age value after adding to brithday variable

print('\n')
# FUNCTIONS - variables within a function only apply to that function - thus
# the name local variable
def who_am_i(): # this is a function without parameters
    name = "Bob" # local variable
    age = 22 # local variable
    print(f"My name is {name}, and I am {age} years old.") # print statement
    # using local variable

who_am_i()

def add_one_hundred(num):
    print(num + 100)

add_one_hundred(100)

def add(x,y):
    print(x + y)

add(7,7)

def multiply(x,y):
    return x * y

print(multiply(7,7))

def square_root(x):
    print(x ** .5)

square_root(64)

def nl(): # new line
    print('\n')

nl()
#BOOLEAN EXPRESSIONS (TRUE OR FALSE)
bool1 = True
bool2 = 3*3 == 9
bool3 = False
bool4 = 3*3 != 9

print(bool1,bool2,bool3,bool4)
print(type(bool1))

bool5 = "True"
print(type(bool5))

nl()
#RELATIONAL AND BOOLEAN OPERATORS
greater_than = 7 > 5
less_than = 5 < 7
greater_than_or_equal_to = 7 >= 7
less_than_or_equal_to = 7<= 7

test_and = (7 > 5) and (5 < 7) # true
test_and2 = (7 > 5) and (5 > 7) # false
test_or = (7 > 5) or (5 < 7) # true
test_or2 = (7 > 5) and (5 > 7) # True

test_not = not True # false

nl()
# CONDITIONAL STATEMENTS - if/elif/else
def drink(money):
    if money >= 2:
        return "You've got yourself a drink!"
    else:
        return "No drink for you!"

print(drink(3))
print(drink(1))

def alcohol(age,money):
    if(age >= 21) and (money >= 5):
        return "We're getting a drink!"
    elif (age >= 21) and (money < 5):
        return "Come back with money."
    elif (age < 21) and (money >=5):
        return "Nice try kid."
    else:
        return "You're too young, AND too poor."

print(alcohol(21,5))
print(alcohol(21,4))
print(alcohol(20,5))
print(alcohol(20,4))

nl()
# LISTS - have brackets []
movies = ["The Matrix", "Godzilla 2000", "Friday the 13th", "Interstellar"]

print(movies[0])
print(movies[1])
print(movies[0:3])
print(len(movies))
movies.append("Jaws")
print(movies[-1])
movies.insert(2, "Boondock Saints")
print(movies)
movies.pop()
print(movies)
movies.pop(0)
print(movies)

bob_movies = ['Just go with it', '50 first dates']
our_movies = movies + bob_movies
print(our_movies)

grades = [["Bob", 82], ["Alice", 90], ["Jeff", 72]]
bob_grade = grades[0][1]
alice_grade = grades[1][1]
jeff_grade = grades[2][1]

print(bob_grade)
print(alice_grade)
print(jeff_grade)

nl()
# TUPLES - do not change, use parenthesis
grades = ("a", "b", "c", "d", "f")

print(grades[1])

nl()
# LOOPING 

# FOR LOOP
vegtables = ['carrot', 'cucumber', 'spinach']

for vegtable in vegtables:
    print(vegtable)

# WHILE LOOP - execute as long as True
i = 1

while i < 10:
    print(i)
    i += 1

nl()
# ADVANCED STRINGS
my_name = "Bob"
print(my_name[0])
print(my_name[-1])

sentence = "This is a sentence"
print(sentence[:4])
print(sentence.split()) #delimeter - default is space

sentence_split = sentence.split()
sentence_join = ' '.join(sentence_split)
print(sentence_join)

quote = 'He said "I really, really love cheese pizza"' # can use single quotes
print(quote)
quote = "He said, \"give me all your money\"" # character escape with \
print(quote)

too_much_space = "                hello   world!"
print(too_much_space.strip())

print("A" in "Apple") # True
print("a" in "Apple") # False

letter = "A"
word = "Apple"
print(letter.lower() in word.lower()) # better way of doing this 

movie = "The Dark Knight"
print(f"My favorite movie is {movie}!") # f string
print("My favorite movie is {}!".format(movie)) # format method
print("My favorite movie is %s!" % movie) # percent method

nl()
# DICTIONARIES - key value pairs {}
drinks = {"White Russian": 7, "Old Fashioned": 10, "Lemon Drop": 8} # drink is
# the key, price is the value
print(drinks)

employees = {"Finance": ["Bob", "Linda", "Tina"],
             "IT": ["Gene", "Louise", "Teddy"],
             "HR": ["Jimmy Jr.", "Mort"]}
print(employees)

employees['Legal Department'] = ["Mr. Frond"] # adds new key value pair
print(employees)

employees.update({"Sales": ["Andie", "Ollie"]}) # adds new key value pair
print(employees)

drinks['White Russian'] = 8 # updates a key value pair
print(drinks)

print(drinks.get("White Russian")) # returns just the value of a key
